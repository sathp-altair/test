// Threads_UseCurrentThread.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <thread>

void threadFunction(int nSec) 
{

    std::chrono::seconds waitTime(nSec);
    std::this_thread::sleep_for(waitTime);
    std::cout << "Hello from thread " << std::this_thread::get_id() << "!" << std::endl;
}

int main()
{
    // Number of threads to create
    const int numThreads = 5;

    std::cout << "Parent thread " << std::this_thread::get_id() << "!" << std::endl;

    // Create multiple threads in a loop
    std::thread threads[numThreads];
    for (int i = 0; i < numThreads; ++i) {
        threads[i] = std::thread(threadFunction, 1);
    }

    std::chrono::seconds waitTime(5);
    std::this_thread::sleep_for(waitTime);

    std::cout << "Parent thread before first call" << "!" << std::endl;

    // Call threadFunction on the current thread
    threadFunction(2);

    std::cout << "Parent thread after first call" << "!" << std::endl;

    // Wait for all threads to finish
    for (int i = 0; i < numThreads; ++i) {
        threads[i].join();
    }

    std::cout << "Parent thread compeletion" << "!" << std::endl;

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
